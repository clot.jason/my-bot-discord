import discord
from discord.ext import commands


class SimpleView(discord.ui.View):

    async def disable_all(self):
        for item in self.children:
            item.disabled = True
        await self.message.edit(view=self)

    @discord.ui.button(label="Hello",
                       style=discord.ButtonStyle.success)
    async def hello(self, interaction: discord.Interaction, button: discord.ui.Button):
        await self.disable_all()
        await interaction.response.send_message("World")


class AskMe(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='question')
    async def question(self, ctx):
        view = SimpleView()

        supportServerButton = discord.ui.Button(label='Support Server', style=discord.ButtonStyle.gray,
                                                url='https://discord.com')
        view.add_item(supportServerButton)
        message = await ctx.send(view=view)
        view.message = message


async def setup(bot):
    await bot.add_cog(AskMe(bot))
