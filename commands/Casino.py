import mysql.connector.types
import mysql.connector
from discord.ext import commands

from mydb import InformationDB


class Casino(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='register', aliases=['reg', 'regis'])
    async def register_command(self, ctx):
        user = ctx.message.author
        user_id = user.id

        host = InformationDB.host
        userdb = InformationDB.user
        pw = InformationDB.pw
        db = InformationDB.db

        mydb = mysql.connector.connect(
            host=host,
            user=userdb,
            password=pw,
            database=db
        )

        mycursor = mydb.cursor(buffered=True)

        check_row = "SELECT id_utilisateur FROM Casino WHERE id_utilisateur = %s"
        check_value = ([user_id])
        mycursor.execute(check_row, check_value)

        if mycursor.rowcount == 0:
            sql = "INSERT INTO Casino (id_utilisateur, wallet) VALUES (%s, %s)"
            val = ([user_id, 100])
            mycursor.execute(sql, val)
            await ctx.send(f"Vous avez été enregistré en tant que joueur.")
            print(f"{ctx.message.author} s'est enregistré en tant que joueur.")
        else:
            await ctx.send(f"Vous êtes déjà enregistré en tant que joueur.")

        mydb.commit()

        mycursor.close()
        mydb.close()

    @commands.command(name='cash', aliases=['money', 'chip'])
    async def cash_command(self, ctx):
        user = ctx.message.author
        user_id = user.id

        host = InformationDB.host
        userdb = InformationDB.user
        pw = InformationDB.pw
        db = InformationDB.db

        mydb = mysql.connector.connect(
            host=host,
            user=userdb,
            password=pw,
            database=db
        )

        mycursor = mydb.cursor(buffered=True)

        request = "SELECT wallet FROM Casino WHERE id_utilisateur = %s"
        check_value = ([user_id])
        mycursor.execute(request, check_value)

        row = mycursor.fetchone()

        if row is not None:
            money = row[0]
            print(f"{ctx.message.author} à fait la demande de son wallet. Il contient {money} jetons.")
            await ctx.send(f"Vous possédez {money} jetons dans votre wallet.")



async def setup(bot):
    await bot.add_cog(Casino(bot))
