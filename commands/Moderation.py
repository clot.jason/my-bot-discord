from discord.ext import commands


class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['del', 'clear'])
    @commands.has_any_role('Gérant Sensei', 'Modérateur Sensei')
    async def delete(self, ctx, number: int):
        await ctx.channel.purge(limit=number + 1)
        await ctx.channel.send(f"{number} messages ont été supprimés.", delete_after=3)
        print(number, "messages ont été supprimés dans le salon :", ctx.channel)
        print(" --------------------")

    @delete.error
    async def delete_handler(self, ctx, error):
        if isinstance(error, commands.MissingAnyRole):
            message = f"Vous n'avez pas rôle qui vous permet d'utiliser cette commmande."
        elif isinstance(error, commands.MissingRequiredArgument):
            message = f"Un nombre de messages à supprimer doit être renségné."
        else:
            message = f"Une erreur est survenue lors de l'éxecution de la commande."
        await ctx.send(message, delete_after=5)
        await ctx.message.delete(delay=5)


async def setup(bot):
    await bot.add_cog(Moderation(bot))
