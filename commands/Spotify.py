import discord
from discord import *
from discord.ext import commands


class Spotify(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='spotify', aliases=['spot', 'spo', 'ecoute', 'écoute', 'listen'])
    async def spotify_command(self, ctx, user: discord.Member = None):
        if user == None:
            user = ctx.author
            pass
        print(f"{ctx.author} a demandé la musique de {user}.")
        if user.activities:
            for activity in user.activities:
                if str(activity) == 'Spotify':
                    await ctx.send(f'https://open.spotify.com/track/{activity.track_id}')
                    print(f"Il écoute {activity.title} de {activity.artist}.")
        else:
            await ctx.send(f"{user.display_name} n'écoute aucune musique.")
            print("Il n'écoute aucune musique.")
        print(" --------------------")


async def setup(bot):
    await bot.add_cog(Spotify(bot))
