import asyncio
import discord
from discord.ext import commands
from discord.utils import get

from tokens import mainToken

token = mainToken.token

default_intents = discord.Intents.all()
default_intents.members = True
bot = commands.Bot(command_prefix="!", intents=default_intents)


async def load_extensions():
    cog_files = ['commands.Spotify', 'commands.Moderation', 'commands.Casino']
    for cog_file in cog_files:
        await bot.load_extension(cog_file)
        print(f"{cog_file} chargé avec succés.")


@bot.event
async def on_ready():
    print("Le bot est prêt.")
    await bot.change_presence(activity=discord.Game("In development"))
    print(" --------------------")


@bot.event
async def on_member_join(member):
    member_count = 0
    for member in bot.get_all_members():
        member_count += 1

    print(f"L'utilisateur {member.display_name} a rejoint le serveur !")
    print(" --------------------")
    embed_arrive = discord.Embed(title=f"{member.display_name} à rejoint le serveur !", color=0x444444)
    embed_arrive.add_field(name="Bienvenue à toi !", value="Amuse toi et passe un bon moment.", inline=False)
    embed_arrive.add_field(name="Nombre de membres :", value=member_count, inline=False)
    embed_arrive.set_thumbnail(url=member.avatar)

    await bot.get_channel(851492990123769877).send(embed=embed_arrive)
    role1 = get(member.guild.roles, id=852490112498401290)
    await member.add_roles(role1)
    await bot.get_channel(855553497047236628).edit(name=f"Membres : {member_count}")


@bot.event
async def on_member_remove(member):
    print(f"L'utilisateur {member.display_name} a quitté le serveur.")
    print(" --------------------")
    embed_depart = discord.Embed(title=f"{member.display_name} à quitté le serveur...", color=0xEE1010)
    embed_depart.add_field(name="Passe un bonne journée !", value="On espère te revoir bientôt (ou pas).", inline=False)
    embed_depart.set_thumbnail(url=member.avatar)

    member_count = 0
    for member in bot.get_all_members():
        member_count += 1
    embed_depart.add_field(name="Nombre de membres :", value=member_count, inline=False)

    await bot.get_channel(851492990123769877).send(embed=embed_depart)
    await bot.get_channel(855553497047236628).edit(name=f"Membres : {member_count}")


@bot.event
async def on_raw_reaction_add(payload):
    message_id = payload.message_id

    if message_id == 861594754214789120:
        role1 = get(payload.member.guild.roles, id=851493385361293393)
        role2 = get(payload.member.guild.roles, id=852490112498401290)

        if payload.emoji.name == '✅':
            print(f'Le role \033[1m\033[4m{role1}\033[0m à été ajouté à {payload.member.display_name}')
            print(" --------------------")
            await payload.member.add_roles(role1)
            await payload.member.remove_roles(role2)


async def main():
    async with bot:
        await load_extensions()
        await bot.start(token)


asyncio.run(main())
